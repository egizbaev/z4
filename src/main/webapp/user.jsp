<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ page import="java.util.*,yourpackage.User" %>
<!DOCTYPE html>
<html>
<head>
    <title>Личный кабинет - Приемная комиссия вуза</title>
    <link rel="stylesheet" type="text/css" href="styles/style.css">
</head>
<body>

<%
    // Здесь предполагается, что пользователь уже прошел аутентификацию и его данные хранятся в сессии
    User user = (User)session.getAttribute("user");
    if(user == null) {
        // Если объект пользователя не найден в сессии, перенаправляем на страницу входа
        response.sendRedirect("index.jsp");
        return;
    }
%>

<div id="header">
    <h1>Личный кабинет</h1>
</div>

<!-- Панель навигации или меню -->
<div id="nav">
    <ul>
        <li><a href="index.jsp">Главная</a></li>
        <li><a href="logout.jsp">Выход</a></li>
    </ul>
</div>

<!-- Секция контента -->
<div id="section">
    <h2>Добро пожаловать, <%= user.getFullName() %>!</h2>

    <!-- Блок информации о пользователе -->
    <div id="user-info">
        <h3>Ваша информация:</h3>
        <p>ФИО: <%= user.getFullName() %></p>
        <p>Номер заявления: <%= user.getApplicationNumber() %></p>
        <!-- Добавьте другие необходимые детали -->
    </div>

    <!-- Блок действий пользователя -->
    <div id="user-actions">
        <h3>Доступные действия:</h3>
        <ul>
            <li><a href="apply.jsp">Подать документы</a></li>
            <li><a href="status.jsp">Проверить статус заявления</a></li>
            <li><a href="editProfile.jsp">Редактировать профиль</a></li>
            <!-- Другие действия -->
        </ul>
    </div>
</div>

<!-- Подвал сайта -->
<div id="footer">
    <p>&copy; 2023 Приемная комиссия вуза</p>
</div>

</body>
</html>
