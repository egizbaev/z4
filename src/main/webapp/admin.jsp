<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <title>Кабинет администратора - Приемная комиссия вуза</title>
    <link rel="stylesheet" type="text/css" href="styles/style.css">
</head>
<body>

<div id="header">
    <h1>Кабинет администратора</h1>
</div>

<div id="nav">
    <!-- Навигация по админ-панели -->
    <ul>
        <li><a href="#users">Пользователи</a></li>
        <li><a href="#applications">Заявления</a></li>
        <li><a href="#courses">Программы</a></li>
        <li><a href="#schedule">Расписание</a></li>
    </ul>
</div>

<div id="section">

    <!-- Секция управления пользователями -->
    <table>
        <c:forEach items="${users}" var="user">
            <tr>
                <td>${user.name}</td>
                <td>
                    <button>Изменить</button>
                    <button>Удалить</button>
                </td>
            </tr>
        </c:forEach>
    </table>


    <!-- Секция управления заявлениями -->
    <div id="applications">
        <h2>Управление заявлениями</h2>
        <table>
            <c:forEach items="${applications}" var="application">
                <tr>
                    <td>${application.id}</td>
                    <td>${application.status}</td>
                    <td>
                        <button>Просмотр</button>
                        <button>Редактировать</button>
                    </td>
                </tr>
            </c:forEach>
        </table>
    </div>

    <!-- Секция управления программами -->

    <div id="courses">
        <h2>Управление программами</h2>
        <table>
            <c:forEach items="${courses}" var="course">
                <tr>
                    <td>${course.name}</td>
                    <td>${course.duration}</td>
                    <td>
                        <button>Изменить</button>
                        <button>Удалить</button>
                    </td>
                </tr>
            </c:forEach>
        </table>
    </div>

    <!-- Секция управления расписанием -->
    <div id="schedule">
        <h2>Управление расписанием</h2>
        <table>
            <tr>
                <th>Дата</th>
                <th>Время</th>
                <th>Предмет</th>
                <th>Действия</th>
            </tr>
            <tr>
                <td>01.09.2023</td>
                <td>10:00</td>
                <td>Математика</td>
                <td>
                    <button>Изменить</button>
                    <button>Удалить</button>
                </td>
            </tr>
            <tr>
                <td>02.09.2023</td>
                <td>12:00</td>
                <td>Физика</td>
                <td>
                    <button>Изменить</button>
                    <button>Удалить</button>
                </td>
            </tr>
            <tr>
                <td>03.09.2023</td>
                <td>14:00</td>
                <td>Информатика</td>
                <td>
                    <button>Изменить</button>
                    <button>Удалить</button>
                </td>
            </tr>
        </table>
    </div>

</div>

<div id="footer">
    <p>&copy; 2023 Приемная комиссия вуза</p>
</div>

</body>
</html>
