<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <title>Контакты - Приемная комиссия вуза</title>
    <link rel="stylesheet" type="text/css" href="styles/style.css">
</head>
<body>

<div id="header">
    <h1>Контактная информация</h1>
</div>

<!-- Панель навигации или меню -->
<div id="nav">
    <ul>
        <li><a href="index.jsp">Главная</a></li>
        <li><a href="about.jsp">О нас</a></li>
        <li><a href="contact.jsp">Контакты</a></li>
    </ul>
</div>

<!-- Секция контента -->
<div id="section">
    <h2>Связаться с нами</h2>
    <p>Если у вас есть вопросы, пожалуйста, используйте информацию ниже, чтобы связаться с приемной комиссией нашего вуза.</p>

    <h3>Адрес</h3>
    <p>123456, г. Москва, ул. Образовательная, д. 10</p>

    <h3>Телефон</h3>
    <p>+7 (495) 123-45-67</p>

    <h3>Электронная почта</h3>
    <p>admissions@vuz.ru</p>

    <h3>Контактная форма</h3>
    <form action="sendContact" method="post">
        <div class="form-group">
            <label for="contact-name">Ваше имя:</label>
            <input type="text" id="contact-name" name="name" required>
        </div>
        <div class="form-group">
            <label for="contact-email">Ваш email:</label>
            <input type="email" id="contact-email" name="email" required>
        </div>
        <div class="form-group">
            <label for="contact-message">Сообщение:</label>
            <textarea id="contact-message" name="message" required></textarea>
        </div>
        <div class="form-group">
            <input type="submit" value="Отправить">
        </div>
    </form>
</div>

<!-- Подвал сайта -->
<div id="footer">
    <p>&copy; 2023 Приемная комиссия вуза</p>
</div>

</body>
</html>
