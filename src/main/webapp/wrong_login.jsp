<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
  <title>Ошибка входа - Приемная комиссия вуза</title>
  <link rel="stylesheet" type="text/css" href="styles/style.css">
</head>
<body>

<div id="header">
  <h1>Ошибка входа</h1>
</div>

<!-- Панель навигации или меню -->
<div id="nav">
  <ul>
    <li><a href="index.jsp">Главная</a></li>
    <li><a href="about.jsp">О нас</a></li>
    <li><a href="contact.jsp">Контакты</a></li>
  </ul>
</div>

<!-- Секция контента -->
<div id="section">
  <h2>Вход не выполнен</h2>
  <p>Неправильный логин или пароль. Пожалуйста, попробуйте ещё раз.</p>
  <p><a href="index.jsp">Вернуться к странице входа</a></p>
</div>

<!-- Подвал сайта -->
<div id="footer">
  <p>&copy; 2023 Приемная комиссия вуза</p>
</div>

</body>
</html>
