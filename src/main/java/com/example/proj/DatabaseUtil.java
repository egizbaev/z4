package com.example.proj;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DatabaseUtil {

    public static Connection getConnection() {
        try {
            String url = "jdbc:sqlite:../../db.sqlite";
            Connection conn = DriverManager.getConnection(url);
            return conn;
        } catch (SQLException e) {
            throw new RuntimeException("Ошибка при подключении к базе данных", e);
        }
    }
}

