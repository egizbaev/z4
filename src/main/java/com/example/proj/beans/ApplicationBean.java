package com.example.proj.beans;

import com.example.proj.entities.Application;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Stateless
public class ApplicationBean {

    @PersistenceContext
    private EntityManager entityManager;

    public Application createApplication(Application application) {
        entityManager.persist(application);
        return application;
    }

    public Application findApplication(Long id) {
        return entityManager.find(Application.class, id);
    }

    // Другие методы, связанные с бизнес-логикой для Application
}
