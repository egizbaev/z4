package com.example.proj.beans;

import com.example.proj.entities.User;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Stateless
public class UserBean {

    @PersistenceContext
    private EntityManager entityManager;

    public User createUser(User user) {
        entityManager.persist(user);
        return user;
    }

    public User findUser(Long id) {
        return entityManager.find(User.class, id);
    }

    // Другие методы, связанные с бизнес-логикой для User
}

