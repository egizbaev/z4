package com.example.proj.beans;

import com.example.proj.entities.Course;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Stateless
public class CourseBean {

    @PersistenceContext
    private EntityManager entityManager;

    public Course createCourse(Course course) {
        entityManager.persist(course);
        return course;
    }

    public Course findCourse(Long id) {
        return entityManager.find(Course.class, id);
    }

    // Другие методы, связанные с бизнес-логикой для Course
}
