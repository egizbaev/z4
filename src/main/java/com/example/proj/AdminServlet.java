package com.example.proj;

import com.example.proj.beans.UserBean;
import com.example.proj.entities.Application;
import com.example.proj.entities.Course;
import com.example.proj.entities.User;
import com.example.proj.repositories.ApplicationRepository;
import com.example.proj.repositories.CourseRepository;
import com.example.proj.repositories.UserRepository;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.List;

import javax.ejb.EJB;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "adminServlet", value = "/admin-servlet")
// Аннотация, которая указывает на то, что класс является сервлетом.
// Name - имя сервлета, value - путь, по которому он будет доступен.
public class AdminServlet extends HttpServlet {

    @PersistenceContext
    private EntityManager entityManager;

    @EJB
    private UserBean userBean;

    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        // Метод, который будет вызван при GET запросе на сервлет.

        userBean.findUser(1L);

        // В методе doGet сервлета
        UserRepository userRepository = new UserRepository(entityManager);
        List<User> users = userRepository.findAll();
        request.setAttribute("users", users);

        CourseRepository courseRepository = new CourseRepository(DatabaseUtil.getConnection());
        List<Course> courses = null;
        try {
            courses = courseRepository.findAll();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        request.setAttribute("courses", courses);


        ApplicationRepository applicationRepository = new ApplicationRepository(DatabaseUtil.getConnection());
        List<Application> applications = null;
        try {
            applications = applicationRepository.findAll();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        request.setAttribute("applications", applications);




        // Forward request to admin.jsp
        // Получаем объект RequestDispatcher, который перенаправит запрос на admin.jsp
        RequestDispatcher dispatcher = request.getRequestDispatcher("admin.jsp");
        // Перенаправляем запрос и ответ на страницу admin.jsp
        dispatcher.forward(request, response);
    }
}
