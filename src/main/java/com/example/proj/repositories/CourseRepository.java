package com.example.proj.repositories;

import com.example.proj.entities.Course;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class CourseRepository {
    private Connection connection;

    public CourseRepository(Connection connection) {
        this.connection = connection;
    }

    // Создание (Create)
    public void create(Course course) throws SQLException {
        String sql = "INSERT INTO courses (name, duration) VALUES (?, ?)";
        try (PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, course.getName());
            statement.setString(2, course.getDuration());
            statement.executeUpdate();
        }
    }

    // Чтение по ID (Read)
    public Course find(Long id) throws SQLException {
        String sql = "SELECT * FROM courses WHERE id = ?";
        try (PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setLong(1, id);
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                return new Course(
                        resultSet.getLong("id"),
                        resultSet.getString("name"),
                        resultSet.getString("duration")
                );
            }
            return null;
        }
    }

    // Чтение всех курсов (Read All)
    public List<Course> findAll() throws SQLException {
        List<Course> courses = new ArrayList<>();
        String sql = "SELECT * FROM courses";
        try (Statement statement = connection.createStatement();
             ResultSet resultSet = statement.executeQuery(sql)) {
            while (resultSet.next()) {
                courses.add(new Course(
                        resultSet.getLong("id"),
                        resultSet.getString("name"),
                        resultSet.getString("duration")
                ));
            }
        }
        return courses;
    }

    // Обновление (Update)
    public void update(Course course) throws SQLException {
        String sql = "UPDATE courses SET name = ?, duration = ? WHERE id = ?";
        try (PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, course.getName());
            statement.setString(2, course.getDuration());
            statement.setLong(3, course.getId());
            statement.executeUpdate();
        }
    }

    // Удаление (Delete)
    public void delete(Long id) throws SQLException {
        String sql = "DELETE FROM courses WHERE id = ?";
        try (PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setLong(1, id);
            statement.executeUpdate();
        }
    }
}
