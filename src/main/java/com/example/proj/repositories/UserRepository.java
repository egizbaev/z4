package com.example.proj.repositories;

import com.example.proj.entities.User;

import javax.persistence.EntityManager;
import java.util.List;
import javax.persistence.TypedQuery;

public class UserRepository {
    private EntityManager entityManager;

    public UserRepository(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    // Создание (Create)
    public User create(User user) {
        entityManager.getTransaction().begin();
        entityManager.persist(user);
        entityManager.getTransaction().commit();
        return user;
    }

    // Чтение по ID (Read)
    public User find(Long id) {
        return entityManager.find(User.class, id);
    }

    // Чтение всех пользователей (Read All)
    public List<User> findAll() {
        TypedQuery<User> query = entityManager.createQuery("SELECT u FROM User u", User.class);
        return query.getResultList();
    }

    // Обновление (Update)
    public User update(User user) {
        entityManager.getTransaction().begin();
        User updatedUser = entityManager.merge(user);
        entityManager.getTransaction().commit();
        return updatedUser;
    }

    // Удаление (Delete)
    public void delete(User user) {
        entityManager.getTransaction().begin();
        entityManager.remove(user);
        entityManager.getTransaction().commit();
    }

    // Удаление по ID (Delete by ID)
    public void deleteById(Long id) {
        User user = find(id);
        if (user != null) {
            delete(user);
        }
    }
}
