package com.example.proj.repositories;

import com.example.proj.entities.Application;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class ApplicationRepository {
    private Connection connection;

    public ApplicationRepository(Connection connection) {
        this.connection = connection;
    }

    // Создание (Create)
    public void create(Application application) throws SQLException {
        String sql = "INSERT INTO applications (status) VALUES (?)";
        try (PreparedStatement statement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS)) {
            statement.setString(1, application.getStatus());
            statement.executeUpdate();

            try (ResultSet generatedKeys = statement.getGeneratedKeys()) {
                if (generatedKeys.next()) {
                    application.setId(generatedKeys.getLong(1));
                } else {
                    throw new SQLException("Creating application failed, no ID obtained.");
                }
            }
        }
    }

    // Чтение по ID (Read)
    public Application find(Long id) throws SQLException {
        String sql = "SELECT * FROM applications WHERE id = ?";
        try (PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setLong(1, id);
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                return new Application(
                        resultSet.getLong("id"),
                        resultSet.getString("status")
                );
            }
            return null;
        }
    }

    // Чтение всех заявлений (Read All)
    public List<Application> findAll() throws SQLException {
        List<Application> applications = new ArrayList<>();
        String sql = "SELECT * FROM applications";
        try (Statement statement = connection.createStatement();
             ResultSet resultSet = statement.executeQuery(sql)) {
            while (resultSet.next()) {
                applications.add(new Application(
                        resultSet.getLong("id"),
                        resultSet.getString("status")
                ));
            }
        }
        return applications;
    }

    // Обновление (Update)
    public void update(Application application) throws SQLException {
        String sql = "UPDATE applications SET status = ? WHERE id = ?";
        try (PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, application.getStatus());
            statement.setLong(2, application.getId());
            statement.executeUpdate();
        }
    }

    // Удаление (Delete)
    public void delete(Long id) throws SQLException {
        String sql = "DELETE FROM applications WHERE id = ?";
        try (PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setLong(1, id);
            statement.executeUpdate();
        }
    }
}
